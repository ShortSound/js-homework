function transportationProblem() {

    var consumer = [40, 60, 80, 60],
        provider = [60, 80, 100],
        matrix = [[1, 3, 4, 2], [4, 5, 8, 3], [2, 3, 6, 7]],
        min_val;

    console.log("Результат: ");
    for (var i = 0; i < provider.length; ++i) {
        for (var k = 0; k < consumer.length; ++k) {
            min_val = Math.min(provider[i], consumer[k]);
            matrix[i][k] = min_val;
            provider[i] -= min_val;
            consumer[k] -= min_val;
        }
    }
    for (var i = 0; i < matrix.length; i++) {
        console.log(matrix[i]);
    }
}


function arrSort() {
    var nums = [];
//       		Array	Generation 
    var startArrGeneration = new Date() * 1000;
    for (var i = 0; i < 1000000; i++) {
        nums[i] = Math.floor(Math.random() * 100);
    }
    var endArrGeneration = new Date() * 1000;
    var sumOfNums = nums.reduce(function (sum, currentNum) {
        return sum + currentNum;
    });
    console.log(nums);
    console.log("Сума чисел масиву = " + sumOfNums);


    //			Up 	  Sorting
    var startUpSorting = new Date() * 1000,
        upSortedArr = nums.sort(),
        endUpSorting = new Date() * 1000;
    console.log("Відсортовано по зростанню: ");
    console.log(upSortedArr);


//					Down Sorting
    var startDownSorting = new Date() * 1000,
        downSortedArr = nums.reverse(),
        endDownSorting = new Date() * 1000;
    console.log("Відсортовано по спаданню: ");
    console.log(downSortedArr);

//					Console time
    console.log("Витрачено часу: ");
    console.log({
        "generation": endArrGeneration - startArrGeneration,
        "upSorting": endUpSorting - startUpSorting,
        "downSorting": endDownSorting - startDownSorting
    })

}
